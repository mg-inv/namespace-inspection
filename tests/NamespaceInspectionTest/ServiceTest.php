<?php
namespace NamespaceInspectionTest;

use Exception;
use NamespaceInspection\PhpClassEntity;
use NamespaceInspection\Service;
use PHPUnit\Framework\TestCase;

/**
 * @covers \NamespaceInspection\Service
 */
class ServiceTest extends TestCase
{
    /**
     * @var Service
     */
    private $service;

    /**
     * @var string
     */
    private $initialNamespace = 'InitialNamespace';

    /**
     * @var string
     */
    private $initialDirectory = 'InitialDirectory';

    /**
     * @var string
     */
    private $absolutePath;

    protected function setUp()
    {
        parent::setUp();

        $this->absolutePath = getcwd() . '/' . $this->initialDirectory;
        mkdir($this->initialDirectory);
        shell_exec("cp -R tests/fixtures/* $this->initialDirectory");

        $this->service = new Service($this->initialDirectory, $this->initialNamespace);
    }

    protected function tearDown()
    {
        parent::tearDown();

        shell_exec("rm -Rf $this->initialDirectory");
    }

    public function testIfThrowsExceptionWhenPathIsNotReal()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Path foo is not real directory');

        new Service('foo','');
    }

    public function testGetters()
    {
        $this->assertSame($this->absolutePath, $this->service->getInitialDirectory());
        $this->assertSame($this->initialNamespace, $this->service->getRootNamespace());
    }

    //start class
    public function testInspectClassName()
    {
        $expectedClass = new PhpClassEntity("CorrectClass", "InitialNamespace");
        $this->assertTrue($this->service->inspectClassName($expectedClass, $this->absolutePath . '/CorrectClass.php'));
    }

    public function testInspectClassNameForTheFileWithoutClass()
    {
        $expectedClass = new PhpClassEntity("", "");
        $this->assertTrue($this->service->inspectClassName($expectedClass, $this->absolutePath . '/someFileWithoutClass.php'));
    }

    public function testInspectClassNameForTwoClasses()
    {
        $file = $this->absolutePath . '/ClassWithTwoNamespaces.php';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(sprintf('File %s contains more that one class', $file));

        $expectedClass = new PhpClassEntity("", "");
        $this->service->inspectClassName($expectedClass, $this->absolutePath . '/ClassWithTwoNamespaces.php');
    }

    public function testInspectClassNameWithIncorrectClassName()
    {
        $file = $this->absolutePath . '/FirstLevel/ClassWithIncorrectNamespace.php';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(sprintf(
            'Actual class name %s does not match expected %s in file %s',
            'ClassWithIncorrectClassName',
            'ClassWithIncorrectNamespace',
            $file
        ));

        $expectedClass = new PhpClassEntity("ClassWithIncorrectNamespace", "InitialNamespace\\FirstLevel");
        $this->service->inspectClassName($expectedClass, $file);
    }
    //end class

    //start namespace
    public function testInspectNamespace()
    {
        $expectedClass = new PhpClassEntity("", "InitialNamespace");
        $this->assertTrue($this->service->inspectNamespace($expectedClass, $this->absolutePath . '/CorrectClass.php'));
        $expectedClass = new PhpClassEntity("", "InitialNamespace\\FirstLevel");
        $this->assertTrue($this->service->inspectNamespace($expectedClass, $this->absolutePath . '/FirstLevel/Test2.php'));
    }

    public function testInspectClassWithoutNamespace()
    {
        $expectedClass = new PhpClassEntity("", "");
        $this->assertTrue($this->service->inspectNamespace($expectedClass, $this->absolutePath . '/ClassWithoutNamespace.php'));
    }

    public function testInspectClassWithTwoNamespaces()
    {
        $file = $this->absolutePath . '/ClassWithTwoNamespaces.php';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(sprintf('File %s contains more than one namespace', $file));

        $expectedClass = new PhpClassEntity("", "");
        $this->service->inspectNamespace($expectedClass, $file);
    }

    public function testInspectClassWithIncorrectNamespace()
    {
        $file = $this->absolutePath . '/FirstLevel/ClassWithIncorrectNamespace.php';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(sprintf(
            'Expected namespace was %s but actual is %s in class %s',
            'InitialNamespace\\FirstLevel',
            'InitialNamespace\\SomeIncorrectNamespace',
            'ClassWithIncorrectNamespace'
        ));

        $expectedClass = new PhpClassEntity("ClassWithIncorrectNamespace", "InitialNamespace\\FirstLevel");
        $this->service->inspectNamespace($expectedClass, $file);
    }
    //end namespace

    public function testCalculateClassEntityBasedOnPhysicalPath()
    {
        $currentPath = $this->absolutePath . '/Qux.php';
        $expectedClass = new PhpClassEntity('Qux', 'InitialNamespace');
        $this->assertEquals(
            $expectedClass,
            $this->service->calculateClassEntityBasedPhysicalPath($currentPath)
        );


        $currentPath = $this->absolutePath . '/Baz/Qux.php';
        $expectedClass = new PhpClassEntity('Qux', 'InitialNamespace\\Baz');
        $this->assertEquals(
            $expectedClass,
            $this->service->calculateClassEntityBasedPhysicalPath($currentPath)
        );
    }
}
