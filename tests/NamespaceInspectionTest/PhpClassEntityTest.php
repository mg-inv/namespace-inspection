<?php
namespace NamespaceInspectionTest;

use NamespaceInspection\PhpClassEntity;
use PHPUnit\Framework\TestCase;

/**
 * @covers \NamespaceInspection\PhpClassEntity
 */
class PhpClassEntityTest extends TestCase
{
    /**
     * @var PhpClassEntity
     */
    private $entity;

    private $className = 'foo';

    private $namespace = 'foo\bar\baz';

    protected function setUp()
    {
        parent::setUp();

        $this->entity = new PhpClassEntity($this->className, $this->namespace);
    }

    public function testGetters()
    {
        $this->assertSame($this->className, $this->entity->getClassName());
        $this->assertSame($this->namespace, $this->entity->getNamespace());
        $this->assertSame($this->className . '.php', $this->entity->getFileName());
    }
}
