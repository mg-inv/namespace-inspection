<?php
namespace NamespaceInspectionTest;

use NamespaceInspection\PhpClassEntity;
use NamespaceInspection\Processor;
use NamespaceInspection\Service;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \NamespaceInspection\Processor
 */
class ProcessorTest extends TestCase
{
    /**
     * @var Processor
     */
    private $processor;

    /**
     * @var Service|PHPUnit_Framework_MockObject_MockObject
     */
    private $serviceMock;

    /**
     * @var string
     */
    private $initialDirectory = 'InitialDirectory';

    /**
     * @var string
     */
    private $absolutePath;

    protected function setUp()
    {
        parent::setUp();

        $this->absolutePath = getcwd() . '/' . $this->initialDirectory;
        mkdir($this->initialDirectory);
        shell_exec("cp -R tests/fixtures/ToBeProcessed/* $this->initialDirectory");

        $this->serviceMock = $this->createMock(Service::class);
        $this->processor = new Processor($this->serviceMock);
    }

    protected function tearDown()
    {
        parent::tearDown();

        shell_exec("rm -Rf $this->initialDirectory");
    }

    public function testProcess()
    {
        $className1 = 'Class1';
        $className2 = 'Class2';

        $namespace1 = 'InitialNamespace';
        $namespace2 = 'InitialNamespace\\Test';

        $directory = $this->absolutePath;
        $path1 = $this->absolutePath . '/Class1.php';
        $path2 = $this->absolutePath . '/Test/Class2.php';

        $expectedClass1 = new PhpClassEntity($className1, $namespace1);
        $expectedClass2 = new PhpClassEntity($className2, $namespace2);

        $this->serviceMock->expects($this->once())
            ->method('getInitialDirectory')
            ->willReturn($directory);

        $this->serviceMock
            ->method('calculateClassEntityBasedPhysicalPath')
            ->will($this->returnValueMap([
                [$path1, $expectedClass1],
                [$path2, $expectedClass2],
            ]));

        $this->serviceMock
            ->method('inspectClassName')
            ->will($this->returnValueMap([
                [$expectedClass1, $path1],
                [$expectedClass2, $path2]
            ]));

        $this->serviceMock
            ->method('inspectNamespace')
            ->will($this->returnValueMap([
                [$expectedClass1, $path1],
                [$expectedClass2, $path2]
            ]));

        $this->processor->process();

        $this->expectOutputString("Inspection started ...\n..\nInspection was completed\n");
    }
}
