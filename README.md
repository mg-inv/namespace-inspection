#NAMESPACE INSPECTION

##DESCRIPTION

- Inspect namespaces if they match directory structure
- Inspect class names if they match file name

##USAGE
./vendor/bin/namespace-inspection path-to-directory [initial-namespace]

##EXAMPLES

- ./vendor/bin/namespace-inspection src NamespaceInspection
- ./vendor/bin/namespace-inspection tests NamespaceInspectionTest
- ./vendor/bin/namespace-inspection src
