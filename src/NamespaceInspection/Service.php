<?php
namespace NamespaceInspection;

use Exception;

class Service
{
    /**
     * @var string
     */
    private $initialDirectory;

    /**
     * @var string
     */
    private $rootNamespace;

    public function __construct($initialDirectory, $rootNamespace = '')
    {
        $realPath = realpath($initialDirectory);
        if (!$realPath) {
            throw new Exception(sprintf("Path %s is not real directory", $initialDirectory));
        }
        $this->initialDirectory = $realPath;
        $this->rootNamespace = $rootNamespace;
    }

    /**
     * @return string
     */
    public function getInitialDirectory()
    {
        return $this->initialDirectory;
    }

    /**
     * @return string
     */
    public function getRootNamespace()
    {
        return $this->rootNamespace;
    }

    /**
     * @param PhpClassEntity $expectedClass
     * @param $filePath
     * @return bool
     * @throws Exception
     */
    public function inspectClassName(PhpClassEntity $expectedClass, $filePath)
    {
        $fileContent = file_get_contents($filePath);
        $classes = [];
        $match = preg_match_all('/\n(?:class|interface|trait|abstract class) [a-zA-Z0-9]+/', $fileContent, $classes);

        if ($match === 0) {
            return true;
        }

        if ($match > 1) {
            throw new Exception(sprintf("File %s contains more that one class", $filePath));
        }

        $className = str_replace("\n", '', $classes[0][0]);
        $className = str_replace('abstract class ', '', $className);
        $className = str_replace('class ', '', $className);
        $className = str_replace('interface ', '', $className);
        $className = str_replace('trait ', '', $className);

        if ($className !== $expectedClass->getClassName()) {
            throw new Exception(
                sprintf(
                    'Actual class name %s does not match expected %s in file %s',
                    $className, $expectedClass->getClassName(), $filePath
                )
            );
        }

        return true;
    }

    /**
     * @param PhpClassEntity $classEntity expected class
     * @param $realPath
     * @return bool
     * @throws Exception
     */
    public function inspectNamespace(PhpClassEntity $classEntity, $realPath)
    {
        $fileContent = file_get_contents($realPath);
        $namespaces = [];
        $match = preg_match_all('/\nnamespace [a-zA-Z]+(?:\\\\[a-zA-Z0-9]+)*;/', $fileContent, $namespaces);

        if ($match === 0) {
            return true;
        }

        if ($match > 1) {
            throw new Exception(sprintf('File %s contains more than one namespace', $realPath));
        }

        $actualNamespace = str_replace("\n", "", $namespaces[0][0]);
        $actualNamespace = str_replace("namespace ", "", $actualNamespace);
        $actualNamespace = str_replace(";", "", $actualNamespace);

        if ($classEntity->getNamespace() != $actualNamespace) {
            throw new Exception(
                sprintf(
                    'Expected namespace was %s but actual is %s in class %s',
                    $classEntity->getNamespace(), $actualNamespace, $classEntity->getClassName()
                )
            );
        }

        return true;
    }

    /**
     * @param string $currentPath
     * @return PhpClassEntity
     */
    public function calculateClassEntityBasedPhysicalPath($currentPath)
    {
        $string = str_replace($this->initialDirectory . "/", "" , $currentPath);

        $chunks = explode('/', $string);
        $className = str_replace(".php", "", array_pop($chunks));
        $namespace = $this->rootNamespace;
        if (!empty($chunks)) {
            if ($namespace !== '') {
                $namespace .= '\\';
            }
            $namespace .= implode('\\', $chunks);
        }

        return new PhpClassEntity($className, $namespace);
    }
}
