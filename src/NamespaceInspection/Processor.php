<?php
namespace NamespaceInspection;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

class Processor
{
    /**
     * @var Service
     */
    private $service;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function process()
    {
        echo "Inspection started ...\n";

        $objects = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($this->service->getInitialDirectory()),
            RecursiveIteratorIterator::SELF_FIRST
        );

        $counter = 0;
        /** @var SplFileInfo $object */
        foreach ($objects as $name => $object) {
            if ($object->isFile() && $object->getExtension() === 'php') {
                echo ".";
                if ($counter == 70) {
                    // @codeCoverageIgnoreStart
                    $counter = 0;
                    echo "\n";
                    // @codeCoverageIgnoreEnd
                } else {
                    $counter++;
                }

                $this->inspect($object->getRealPath());
            }
        }

        echo "\nInspection was completed\n";
    }

    private function inspect($path)
    {
        /** @var string $name absolute path to the file */
        $expectedClass = $this->service->calculateClassEntityBasedPhysicalPath($path);

        $this->service->inspectClassName($expectedClass, $path);
        $this->service->inspectNamespace($expectedClass, $path);
    }
}
