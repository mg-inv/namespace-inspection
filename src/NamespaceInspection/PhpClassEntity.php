<?php
namespace NamespaceInspection;

class PhpClassEntity
{
    /**
     * @var string
     */
    private $className = '';

    /**
     * @var string
     */
    private $namespace = '';

    /**
     * @var string
     */
    private $fileName = '';

    /**
     * @param $className
     * @param $namespace
     */
    public function __construct($className, $namespace)
    {
        $this->className = $className;
        $this->namespace = $namespace;
        $this->fileName = $className . '.php';
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }
}
